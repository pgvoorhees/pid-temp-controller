// This is an independent project of an individual developer. Dear PVS-Studio,
// please check it. PVS-Studio Static Code Analyzer for C, C++ and C#:
// http://www.viva64.com

/* Standard includes. */
#include <stdint.h>
#include <stdio.h>
#include <string.h>

/* Hardware Specific Includes */
#include "stm32f4xx.h"
#include "stm32f4xx_gpio.h"
#include "stm32f4xx_rcc.h"
#include "stm32f4xx_spi.h"
#include "stm32f4xx_usart.h"

/* Kernel includes. */
#include "FreeRTOS_Source/include/FreeRTOS.h"
#include "FreeRTOS_Source/include/queue.h"
#include "FreeRTOS_Source/include/semphr.h"
#include "FreeRTOS_Source/include/task.h"
#include "FreeRTOS_Source/include/timers.h"

/* Application includes */
#include "inc/lcd.h"
#include "inc/parse.h"
#include "inc/system.h"
#include "inc/uart.h"

/* Priorities at which the tasks are created.  The event semaphore task is
given the maximum priority of ( configMAX_PRIORITIES - 1 ) to ensure it runs as
soon as the semaphore is given. */
#define mainQUEUE_RECEIVE_TASK_PRIORITY (tskIDLE_PRIORITY + 2)
#define mainQUEUE_SEND_TASK_PRIORITY (tskIDLE_PRIORITY + 1)
#define mainEVENT_SEMAPHORE_TASK_PRIORITY (configMAX_PRIORITIES - 1)

/* The period of the example software timer, specified in milliseconds, and
converted to ticks using the portTICK_RATE_MS constant. */
#define mainSOFTWARE_TIMER_PERIOD_MS (1000 / portTICK_RATE_MS)

// This allocation tied to configAPPLICATION_ALLOCATED_HEAP define
__attribute__((section(".user_heap_stack")))
uint8_t ucHeap[configTOTAL_HEAP_SIZE];

/*-----------------------------------------------------------*/

/*
 * TODO: Implement this function for any hardware specific clock configuration
 * that was not already performed before main() was called.
 */
static void prvSetupHardware(void);

/*
 * The callback function assigned to the example software timer as described at
 * the top of this file.
 */
static void vExampleTimerCallback(xTimerHandle xTimer);

/*-----------------------------------------------------------*/

float pVal = -1.0f;
const lcd_ctrlGrp *lcd_cg1, *lcd_cg2, *lcd_cg3, *lcd_cg4;
const lcd_VltAmpGrp *lcd_vamp_amps, *lcd_vamp_volt;

/*-----------------------------------------------------------*/

int main(void)
{
  xTimerHandle xExampleSoftwareTimer = NULL;

  /* Configure the system ready to run the demo.  The clock configuration
  can be done here if it was not done before main() was called. */
  prvSetupHardware();

  // SWO_Init(0x1, SystemCoreClock);

  lcd_init();

  lcd_cg1 = lcd_CtlGrp_init(LCD_CG1);
  lcd_cg2 = lcd_CtlGrp_init(LCD_CG2);
  lcd_cg3 = lcd_CtlGrp_init(LCD_CG3);
  lcd_cg4 = lcd_CtlGrp_init(LCD_CG4);
  lcd_vamp_amps = lcd_VAMP_init(LCD_VAMP_A);
  lcd_vamp_volt = lcd_VAMP_init(LCD_VAMP_V);

  /* Create the software timer as described in the comments at the top of
  this file.  http://www.freertos.org/FreeRTOS-timers-xTimerCreate.html. */
  xExampleSoftwareTimer = xTimerCreate(
      "LEDTimer",                   /* A text name, purely to help debugging. */
      mainSOFTWARE_TIMER_PERIOD_MS, /* The timer period, in this case 1000ms
                                        (1s). */
      pdTRUE,                       /* This is a periodic timer, so xAutoReload is set to pdTRUE. */
      (void *)0,                    /* The ID is not used, so can be set to anything. */
      vExampleTimerCallback         /* The callback function that switches the LED off.
                              */
  );

  // char helloWorld[] = "HelloWorld!";

  // SWO_PrintString(&helloWorld[0], 0);
  /* Start the created timer.  A block time of zero is used as the timer
  command queue cannot possibly be full here (this is the first timer to
  be created, and it is not yet running).
  http://www.freertos.org/FreeRTOS-timers-xTimerStart.html */
  xTaskCreate(lcdTask, "LCD Task", 512, NULL, 1, NULL);
  xTimerStart(xExampleSoftwareTimer, 0);
  uart_Initialize( 2, NULL, NULL);

  /* Start the tasks and timer running. */
  vTaskStartScheduler();

  /* If all is well, the scheduler will now be running, and the following line
  will never be reached.  If the following line does execute, then there was
  insufficient FreeRTOS heap memory available for the idle and/or timer tasks
  to be created.  See the memory management section on the FreeRTOS web site
  for more details.  http://www.freertos.org/a00111.html */
  for (;;)
    ;
}
/*-----------------------------------------------------------*/

static void vExampleTimerCallback(xTimerHandle xTimer)
{
  /* The timer has expired.  Count the number of times this happens.  The
  timer that calls this function is an auto re-load timer, so it will
  execute periodically. http://www.freertos.org/RTOS-software-timer.html */
  // GPIO_ToggleBits(GPIOB, GPIO_Pin_14 | GPIO_Pin_7);
  // lcd_cg1->gfx(LCD_ICON_LV2, OFF);
  // lcd_cg4->gfx(LCD_ICON_LV1, OFF);
  lcd_cg1->heat(TOG);
  lcd_cg1->pVal(0.0f);
  lcd_cg1->sVal(pVal);
  lcd_cg2->heat(TOG);
  lcd_cg2->pVal(0.0f);
  lcd_cg2->sVal(pVal);
  lcd_cg3->heat(TOG);
  lcd_cg3->pVal(0.0f);
  lcd_cg3->sVal(pVal);
  lcd_cg4->heat(TOG);
  lcd_cg4->pVal(0.0f);
  lcd_cg4->sVal(pVal);
  lcd_vamp_amps->val(1.1);
  lcd_vamp_volt->val(pVal);

  pVal += 0.1f;
}
/*-----------------------------------------------------------*/

void vApplicationTickHook(void)
{
  /* The RTOS tick hook function is enabled by setting configUSE_TICK_HOOK to
  1 in FreeRTOSConfig.h.*/
  return;
}
/*-----------------------------------------------------------*/

void vApplicationMallocFailedHook(void)
{
  /* The malloc failed hook is enabled by setting
  configUSE_MALLOC_FAILED_HOOK to 1 in FreeRTOSConfig.h.

  Called if a call to pvPortMalloc() fails because there is insufficient
  free memory available in the FreeRTOS heap.  pvPortMalloc() is called
  internally by FreeRTOS API functions that create tasks, queues, software
  timers, and semaphores.  The size of the FreeRTOS heap is set by the
  configTOTAL_HEAP_SIZE configuration constant in FreeRTOSConfig.h. */
  for (;;)
    ;
}
/*-----------------------------------------------------------*/

void vApplicationStackOverflowHook(xTaskHandle pxTask,
                                   signed char *pcTaskName)
{
  (void)pcTaskName;
  (void)pxTask;

  /* Run time stack overflow checking is performed if
  configconfigCHECK_FOR_STACK_OVERFLOW is defined to 1 or 2.  This hook
  function is called if a stack overflow is detected.  pxCurrentTCB can be
  inspected in the debugger if the task name passed into this function is
  corrupt. */
  for (;;)
    ;
}
/*-----------------------------------------------------------*/

void vApplicationIdleHook(void)
{
  volatile size_t xFreeStackSpace;

  /* The idle task hook is enabled by setting configUSE_IDLE_HOOK to 1 in
  FreeRTOSConfig.h.

  This function is called on each cycle of the idle task.  In this case it
  does nothing useful, other than report the amount of FreeRTOS heap that
  remains unallocated. */
  xFreeStackSpace = xPortGetFreeHeapSize();

  if (xFreeStackSpace > 100)
  {
    /* By now, the kernel has allocated everything it is going to, so
    if there is a lot of heap remaining unallocated then
    the value of configTOTAL_HEAP_SIZE in FreeRTOSConfig.h can be
    reduced accordingly. */
  }
}
/*-----------------------------------------------------------*/

static void prvSetupHardware(void)
{
  /* Ensure all priority bits are assigned as preemption priority bits.
  http://www.freertos.org/RTOS-Cortex-M3-M4.html */
  NVIC_SetPriorityGrouping(0);
  SystemCoreClockUpdate();

  /* TODO: Setup the clocks, etc. here, if they were not configured before
  main() was called. */
  RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOB, ENABLE);
  GPIO_InitTypeDef leds = {
      .GPIO_Pin = GPIO_Pin_7 | GPIO_Pin_14 | GPIO_Pin_0,
      .GPIO_Mode = GPIO_Mode_OUT,
      .GPIO_Speed = GPIO_Speed_2MHz,
      .GPIO_OType = GPIO_OType_PP,
      .GPIO_PuPd = GPIO_PuPd_NOPULL,
  };
  GPIO_Init(GPIOB, &leds);
  GPIO_SetBits(GPIOB, GPIO_Pin_14);
}
