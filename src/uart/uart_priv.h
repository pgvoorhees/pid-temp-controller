#ifndef __UART_PRIV
#define __UART_PRIV

#include "stm32f4xx.h"
#include <string.h>

#include "FreeRTOS.h"
#include "event_groups.h"
#include "message_buffer.h"
#include "task.h"

#include "uart.h"
#include "system.h"

#define HWPERIPH USART3
#define USART_IRQ USART3_IRQn

#define RTOS_STACK_DEPTH (256)
#define UART_BUFFER_SZ (128)
#define UART_BUFFER_CT (3)
#define UART_BUFFER_MEM_ALLOC (UART_BUFFER_SZ * UART_BUFFER_CT)

#define evt_RxReady (1 << 0)
#define evt_RxMsgSent ((1<<1))
// #define evt_RxDMACycle ((1<<2))
// #define evt_RxReady ((1<<3))

typedef struct
{
  // Peripheral State Variables
  USART_InitTypeDef xmitConfig;
  DMA_InitTypeDef dmaTxConfig;
  DMA_InitTypeDef dmaRxConfig;

  // RTOS State variables
  EventGroupHandle_t hwEvents;
  MessageBufferHandle_t rxMessageBuffer;
  uint8_t rxPeriphBuffer[UART_BUFFER_MEM_ALLOC]; // For DMA
  MessageBufferHandle_t txMessageBuffer;
  uint8_t txPeriphBuffer[UART_BUFFER_MEM_ALLOC]; // For DMA
} uart_state_t;

void uart_mgrTask(void* params);
void uart_RxTask(void* params);
void uart_TxTask(void* params);

#endif // UART_PRIV
