// This is an independent project of an individual developer. Dear PVS-Studio,
// please check it. PVS-Studio Static Code Analyzer for C, C++ and C#:
// http://www.viva64.com

//		Filename: uart.c
//		Author: Peter Voorhees
//		Project: TempCo
//		Copyright 2019

#include "uart_priv.h"

uart_state_t uart_state;

void uart_Initialize(
    UBaseType_t prio, TaskHandle_t* hnd_uartTx, TaskHandle_t* hnd_uartRx)
{
    RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOD, ENABLE);
    // Enable GPIOD and configure the pins for USART3
    GPIO_InitTypeDef dbgUSART = {
        .GPIO_Pin = GPIO_Pin_8 | GPIO_Pin_9,
        .GPIO_Mode = GPIO_Mode_AF,
        .GPIO_Speed = GPIO_Speed_25MHz,
        .GPIO_OType = GPIO_OType_PP,
        .GPIO_PuPd = GPIO_PuPd_NOPULL,
    };

    GPIO_Init(GPIOD, &dbgUSART);

    GPIO_PinAFConfig(GPIOD, GPIO_PinSource8, GPIO_AF_USART3);
    GPIO_PinAFConfig(GPIOD, GPIO_PinSource9, GPIO_AF_USART3);

    // Enable USART
    RCC_APB1PeriphClockCmd(RCC_APB1Periph_USART3, ENABLE);

    // Configure the BUS characteristics. Defaults are basically fine.
    USART_StructInit(&uart_state.xmitConfig);
    uart_state.xmitConfig.USART_BaudRate = 115200;
    USART_Init(HWPERIPH, &uart_state.xmitConfig);

    USART_DMACmd(HWPERIPH, USART_DMAReq_Rx | USART_DMAReq_Tx, ENABLE);
    USART_Cmd(HWPERIPH, ENABLE);

    uart_state.rxMessageBuffer = xMessageBufferCreate(UART_BUFFER_SZ);
    uart_state.txMessageBuffer = xMessageBufferCreate(UART_BUFFER_SZ);
    xTaskCreate(
        uart_RxTask, "UART Rx", RTOS_STACK_DEPTH, NULL, prio, hnd_uartRx);
    xTaskCreate(
        uart_TxTask, "UART Tx", RTOS_STACK_DEPTH, NULL, prio, hnd_uartTx);
    // xTaskCreate(
    //     uart_mgrTask, "UART Mgr", RTOS_STACK_DEPTH, NULL, prio, hnd_uartTx);
    uart_state.hwEvents = xEventGroupCreate();
}

// void uart_mgrTask(void* params)
// {
//     uint8_t msgBuf[UART_BUFFER_SZ] = {0};
//     for (;;) {
//         size_t bytesRx = 0;
//         while (!(evt_RxMsgSent
//             & xEventGroupWaitBits(uart_state.hwEvents, evt_RxMsgSent, pdTRUE,
//                   pdTRUE, pdMS_TO_TICKS(10)))) {
//             __asm("NOP;");
//         }
        


//     }
// }

// Generates the RXready event when an IDLE flag is detected.
void USART3_IRQHandler(void)
{
    USART_GetFlagStatus(HWPERIPH, USART_FLAG_IDLE);
    USART_ReceiveData(HWPERIPH);
    BaseType_t higherPriorityTaskWoken = pdFALSE;
    xEventGroupSetBitsFromISR(
        uart_state.hwEvents, evt_RxReady, &higherPriorityTaskWoken);
}
// Generates the RXready even when a full buffer is detected.
void DMA1_Stream1_IRQHandler(void)
{
    BaseType_t higherPriorityTaskWoken = pdFALSE;
    if (DMA_GetFlagStatus(DMA1_Stream1, DMA_FLAG_TCIF1) == SET) {
        DMA_ClearFlag(DMA1_Stream1, DMA_FLAG_TCIF1);
    }
}

void uart_RxTask(void* params)
{
    // Configure the UART interrupt for IDLE line detection
    NVIC_SetPriority(USART_IRQ, 0x7);
    USART_ITConfig(HWPERIPH, USART_IT_IDLE, ENABLE);
    NVIC_EnableIRQ(USART_IRQ);

    // Configure the DMA channels.
    // USART3 uses RX:Stream2:Ch4, TX:Stream3:Ch4,4:Ch7
    // Configuring for RX:S1C4, TX:S3:C4
    RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_DMA1, ENABLE);
    DMA_StructInit(&uart_state.dmaRxConfig);
    uart_state.dmaRxConfig.DMA_Channel = DMA_Channel_4;
    uart_state.dmaRxConfig.DMA_PeripheralBaseAddr = (uint32_t) & (HWPERIPH->DR);
    uart_state.dmaRxConfig.DMA_Memory0BaseAddr
        = (uint32_t)&uart_state.rxPeriphBuffer[0];
    uart_state.dmaRxConfig.DMA_MemoryInc = DMA_MemoryInc_Enable;
    uart_state.dmaRxConfig.DMA_DIR = DMA_DIR_PeripheralToMemory;
    uart_state.dmaRxConfig.DMA_BufferSize = UART_BUFFER_MEM_ALLOC;
    uart_state.dmaRxConfig.DMA_Mode = DMA_Mode_Circular;
    DMA_Init(DMA1_Stream1, &uart_state.dmaRxConfig);
    DMA_Cmd(DMA1_Stream1, ENABLE);

    // Configure DMA stream to interrupt if we get a full buffer.
    DMA_ITConfig(DMA1_Stream1, DMA_IT_TC, ENABLE);
    NVIC_EnableIRQ(DMA1_Stream1_IRQn);

    uint8_t tempBuf[UART_BUFFER_SZ] = {0};
    int16_t lastRx = UART_BUFFER_MEM_ALLOC;
    int16_t DDCsnapshot = 0;
    int16_t bytesRx = 0;
    size_t msgBeginIdx = 0;
    int32_t evtFlags = 0;
    uint8_t *srcBuffer;
    for (;;) {

        do {
            evtFlags = xEventGroupWaitBits(uart_state.hwEvents, evt_RxReady,
                pdTRUE, pdTRUE, pdMS_TO_TICKS(10));
        } while (!(evt_RxReady & evtFlags));

        // Normally lastRx will be > DMADataCounter (DDC), which counts down.
        DDCsnapshot = DMA_GetCurrDataCounter(DMA1_Stream1);
        bytesRx = lastRx - DDCsnapshot;
        lastRx = DDCsnapshot;

        // Definitely wrapped the buffer.
        // In this case, lastRx will be < DDC and bytesRx will be very
        // negative.
        // Eg: lastRx = 20, DDC = 354; Rx'd bytes = 50.
        // bytesRx will be (20-354) = -334
        // Using the UART_BUFF_MEM_ALLOC adjustment: (-334+384) = 50
        if (bytesRx < 0)
            bytesRx += UART_BUFFER_MEM_ALLOC;

        msgBeginIdx = UART_BUFFER_MEM_ALLOC - lastRx - bytesRx;

        // Same type of deal as above.
        if(msgBeginIdx < 0)
            msgBeginIdx += UART_BUFFER_MEM_ALLOC;

        // Now that we have the start and end locations. Linearize buffer
        // if necessary. Otherwise, it's already linear.
        if(msgBeginIdx > lastRx){
            memcpy(&tempBuf[0], 
                &uart_state.rxPeriphBuffer[msgBeginIdx],
                UART_BUFFER_MEM_ALLOC - msgBeginIdx);
            memcpy(&tempBuf[UART_BUFFER_MEM_ALLOC - msgBeginIdx],
                &uart_state.rxPeriphBuffer[0],
                bytesRx - UART_BUFFER_MEM_ALLOC + lastRx);
            srcBuffer = &tempBuf[0];
        } else {
            srcBuffer = &uart_state.rxPeriphBuffer[msgBeginIdx];
        }

        // Send the message off in to the Aether for processing
        xMessageBufferSend(
            uart_state.txMessageBuffer, srcBuffer, bytesRx, pdMS_TO_TICKS(10));
        // Fire off event in case anyone cares.
        xEventGroupSetBits(uart_state.hwEvents, evt_RxMsgSent);
    }
}

void uart_TxTask(void* params)
{

    // Configure the DMA channels.
    // USART3 uses TX:Stream3:Ch4,4:Ch7
    // Configuring for TX:S3:C4
    // Init the TX structure for things that aren't going to change.
    DMA_StructInit(&uart_state.dmaTxConfig);
    uart_state.dmaTxConfig.DMA_Channel = DMA_Channel_4;
    uart_state.dmaTxConfig.DMA_PeripheralBaseAddr = (uint32_t) & (HWPERIPH->DR);
    uart_state.dmaTxConfig.DMA_MemoryInc = DMA_MemoryInc_Enable;
    uart_state.dmaTxConfig.DMA_DIR = DMA_DIR_MemoryToPeripheral;
    uart_state.dmaTxConfig.DMA_Priority = DMA_Priority_Low;
    uart_state.dmaTxConfig.DMA_BufferSize = UART_BUFFER_MEM_ALLOC;
    DMA_ITConfig(
        DMA1_Stream3, DMA_IT_TC | DMA_IT_HT | DMA_IT_TE | DMA_IT_FE, ENABLE);
    NVIC_EnableIRQ(DMA1_Stream3_IRQn);

    uint8_t txBytes[UART_BUFFER_SZ];
    size_t bytesToTx = 0;
    for (;;) {

        do {
            bytesToTx = xMessageBufferReceive(uart_state.txMessageBuffer,
                txBytes, UART_BUFFER_SZ, pdMS_TO_TICKS(10));
        } while (!bytesToTx);

        uart_state.dmaTxConfig.DMA_BufferSize = bytesToTx;
        uart_state.dmaTxConfig.DMA_Memory0BaseAddr = (uint32_t)txBytes;
        DMA_Init(DMA1_Stream3, &uart_state.dmaTxConfig);
        USART_ClearFlag(HWPERIPH, USART_FLAG_TC);
        DMA_Cmd(DMA1_Stream3, ENABLE);
        // switch()
    }
}

void DMA1_Stream3_IRQHandler(void)
{
    DMA_ClearFlag(DMA1_Stream3,
        DMA_FLAG_FEIF3 | DMA_FLAG_DMEIF3 | DMA_FLAG_TEIF3 | DMA_FLAG_HTIF3
            | DMA_FLAG_TCIF3);
}
