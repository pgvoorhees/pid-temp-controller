// This is an independent project of an individual developer. Dear PVS-Studio,
// please check it. PVS-Studio Static Code Analyzer for C, C++ and C#:
// http://www.viva64.com

//		Filename: system.c
//		Author: Peter Voorhees
//		Project: TempCo
//		Copyright 2019

#include "system_priv.h"

#include "core_cm4.h"

/*!
 * \brief Initialize the SWO trace port for debug message printing
 * \param portBits Port bit mask to be configured
 * \param cpuCoreFreqHz CPU core clock frequency in Hz
 */
void SWO_Init(uint32_t portBits, uint32_t cpuCoreFreqHz) {
  /* default 64k baud rate */
  uint32_t SWOSpeed = 64000;
  // uint32_t SWOSpeed = 1800000;
  /* SWOSpeed in Hz, note that cpuCoreFreqHz is expected to be match the CPU
   * core clock */
  uint32_t SWOPrescaler = (cpuCoreFreqHz / SWOSpeed) - 1;

  /* enable trace in core debug */
  CoreDebug->DEMCR = CoreDebug_DEMCR_TRCENA_Msk;
  /* "Selected PIN Protocol Register": Select which protocol to use for trace
   * output (2: SWO NRZ, 1: SWO Manchester encoding) */
  *((volatile unsigned *)(ITM_BASE + 0x400F0)) = 0x00000002;
  /* "Async Clock Prescaler Register". Scale the baud rate of the asynchronous
   * output */
  *((volatile unsigned *)(ITM_BASE + 0x40010)) = SWOPrescaler;
  /* ITM Lock Access Register, C5ACCE55 enables more write access to Control
   * Register 0xE00 :: 0xFFC */
  *((volatile unsigned *)(ITM_BASE + 0x00FB0)) = 0xC5ACCE55;
  /* ITM Trace Control Register */
  ITM->TCR = ITM_TCR_TraceBusID_Msk | ITM_TCR_SWOENA_Msk | ITM_TCR_SYNCENA_Msk |
             ITM_TCR_ITMENA_Msk;
  /* ITM Trace Privilege Register */
  ITM->TPR = ITM_TPR_PRIVMASK_Msk;
  /* ITM Trace Enable Register. Enabled tracing on stimulus ports. One bit per
   * stimulus port. */
  ITM->TER = portBits;
  /* DWT_CTRL */
  *((volatile unsigned *)(ITM_BASE + 0x01000)) = 0x400003FE;
  /* Formatter and Flush Control Register */
  *((volatile unsigned *)(ITM_BASE + 0x40304)) = 0x00000100;
}

/*!
 * \brief Sends a character over the SWO channel
 * \param c Character to be sent
 * \param portNo SWO channel number, value in the range of 0 to 31
 */
void SWO_PrintChar(char c, uint8_t portNo) {
  volatile int timeout;

  /* Check if Trace Control Register (ITM->TCR at 0xE0000E80) is set */
  /* check Trace Control Register if ITM trace is enabled*/
  if ((ITM->TCR & ITM_TCR_ITMENA_Msk) == 0) {
    return; /* not enabled? */
  }
  /* Check if the requested channel stimulus port (ITM->TER at 0xE0000E00) is
   * enabled */
  /* check Trace Enable Register if requested port is enabled */
  if ((ITM->TER & (1ul << portNo)) == 0) {
    return; /* requested port not enabled? */
  }
  timeout = 5000; /* arbitrary timeout value */
  while (ITM->PORT[0].u32 == 0) {
    /* Wait until STIMx is ready, then send data */
    timeout--;
    if (timeout == 0) {
      return; /* not able to send */
    }
  }
  // ITM->PORT[0].u16 = 0x08 | (c << 8);
  ITM->PORT[0].u8 = c;
}

/*!
 * \brief Sends a string over SWO to the host
 * \param s String to send
 * \param portNumber Port number, 0-31, use 0 for normal debug strings
 */
void SWO_PrintString(const char *s, uint8_t portNumber) {
  while (*s != '\0') {
    // SWO_PrintChar(*s++, portNumber);
    // ITM_SendChar(*s++);
  }
}
