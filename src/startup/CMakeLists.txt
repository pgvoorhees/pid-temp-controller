target_sources("${PROJECT_NAME}"
    # Project Boilerplate

    PUBLIC "startup_stm32f413xx.s"
    PUBLIC "stm32f4xx_it.c"
    PUBLIC "syscalls.c"
    PUBLIC "system_stm32f4xx.c"
)