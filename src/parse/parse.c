// This is an independent project of an individual developer. Dear PVS-Studio,
// please check it. PVS-Studio Static Code Analyzer for C, C++ and C#:
// http://www.viva64.com

//		Filename: parse.c
//		Author: Peter Voorhees
//		Project: TempCo
//		Copyright 2019

#include "parse.h"
#include "parse_priv.h"

Parse_State_t parse_state;

// void parseInit(UBaseType_t prio, TaskHandle_t *feedBackHandle) {
//   xTaskCreate(
//       parseTask, "Parser Task", PARSE_TASK_STACK_SIZE,
//       NULL, // For right now. If the task needs init params, it goes here
//       prio, feedBackHandle);
// }
// void parseTask(void *params) {}
// QueueHandle_t parse_GetCmdQ() { return parse_state.commandQ; }
