//		Filename: parse_priv.h
//		Author: Peter Voorhees
//		Project: TempCo
//		Copyright 2019

#ifndef __PARSE_PRIV
#define __PARSE_PRIV

#include "inc/parse.h"
#include "inc/stm32f4xx_conf.h"

#include "FreeRTOS.h"
#include "queue.h"
#include "task.h"

#define PARSE_TASK_STACK_WORD_COUNT 100

#define PARSE_TASK_STACK_SIZE ((PARSE_TASK_STACK_WORD_COUNT) * sizeof(int32_t))

typedef struct {
  QueueHandle_t commandQ;  // The queue an incomming command is using
  QueueHandle_t responseQ; // The queue used to return a response.
} Parse_State_t;

void parseTask(void *params);

#endif // PARSE_PRIV
