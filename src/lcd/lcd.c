// This is an independent project of an individual developer. Dear PVS-Studio,
// please check it. PVS-Studio Static Code Analyzer for C, C++ and C#:
// http://www.viva64.com

#include "lcd_priv.h"

#pragma region LCD Control Group Creation Macro
#define __LCD_CONTROL_GROUP_ROUTING_SHIMS(name)                                \
  inline void _lcd_##name##_disp(Lcd_seg_state state) {                        \
    _lcd_CGdisp(&__lcd_##name, state);                                         \
  }                                                                            \
  inline void _lcd_##name##_heat(Lcd_seg_state state) {                        \
    _lcd_control_group_data *tmp =                                             \
        (_lcd_control_group_data *)__lcd_##name.priv;                          \
    _lcd_iconSet(&tmp->heat, state);                                           \
  }                                                                            \
  inline void _lcd_##name##_gfx(Lcd_Gfx seg, Lcd_seg_state st) {               \
    assert(seg <= LCD_ICON_LV3);                                               \
    _lcd_iconSet(&__lcd_##name##_privData.gfx[(int)seg], st);                  \
  }                                                                            \
  inline void _lcd_##name##_indicator(Lcd_Ico_Cmd_CtlGrp ico,                  \
                                      Lcd_seg_state st) {                      \
    const _lcd_icon *tmp;                                                      \
    switch (ico) {                                                             \
    case UNITS:                                                                \
      tmp = &__lcd_##name##_privData.unit;                                     \
      break;                                                                   \
    case PVAL:                                                                 \
      tmp = &__lcd_##name##_privData.pv_ico;                                   \
      break;                                                                   \
    case SVAL:                                                                 \
      tmp = &__lcd_##name##_privData.sv_ico;                                   \
      break;                                                                   \
    }                                                                          \
    _lcd_iconSet(tmp, st);                                                     \
  }                                                                            \
  inline void _lcd_##name##_pval(float val) {                                  \
    _lcd_control_group_data *tmp =                                             \
        (_lcd_control_group_data *)__lcd_##name.priv;                          \
    _lcd_val_1DP(tmp->pv_digits, &tmp->pv_dp, val);                            \
    _lcd_##name##_indicator(PVAL, ON);                                         \
  }                                                                            \
  inline void _lcd_##name##_sval(float val) {                                  \
    _lcd_control_group_data *tmp =                                             \
        (_lcd_control_group_data *)__lcd_##name.priv;                          \
    _lcd_val_1DP(tmp->sv_digits, &tmp->sv_dp, val);                            \
    _lcd_##name##_indicator(SVAL, ON);                                         \
  }

// Just in case we need a fully worked out version of the above macro for "cg1"
// inline void _lcd_cg1_disp(Lcd_seg_state state) {
//   _lcd_CGdisp(&__lcd_cg1, state);
// }
// inline void _lcd_cg1_heat(Lcd_seg_state state) {
//   _lcd_control_group_data *tmp = (_lcd_control_group_data *)__lcd_cg1.priv;
//   _lcd_iconSet(&tmp->heat, state);
// }
// inline void _lcd_cg1_gfx(Lcd_Gfx seg, Lcd_seg_state st) {
//   assert(seg <= LCD_ICON_LV3);
//   _lcd_iconSet(&__lcd_cg1_privData.gfx[seg], st);
// }
// inline void _lcd_cg1_indicator(Lcd_Ico_Cmd_CtlGrp ico, Lcd_seg_state st) {
//   const _lcd_icon *tmp;
//   switch (ico) {
//   case UNITS:
//     tmp = &__lcd_cg1_privData.unit;
//     break;
//   case PVAL:
//     tmp = &__lcd_cg1_privData.pv_ico;
//     break;
//   case SVAL:
//     tmp = &__lcd_cg1_privData.sv_ico;
//     break;
//   }
//   _lcd_iconSet(tmp, st);
// }
// inline void _lcd_cg1_pval(float val) {
//   _lcd_control_group_data *tmp = (_lcd_control_group_data *)__lcd_cg1.priv;
//   _lcd_val_1DP(tmp->pv_digits, &tmp->pv_dp, val);
//   _lcd_cg1_indicator(PVAL, ON);
// }
// inline void _lcd_cg1_sval(float val) {
//   _lcd_control_group_data *tmp = (_lcd_control_group_data *)__lcd_cg1.priv;
//   _lcd_val_1DP(tmp->sv_digits, &tmp->sv_dp, val);
//   _lcd_cg1_indicator(SVAL, ON);
// }

#pragma endregion LCD Control Group Creation Macro

inline void _lcd_V_disp(Lcd_seg_state state) {
  // TODO: Implement
  // _lcd_CGdisp(&__lcd_V, state);
}
inline void _lcd_V_val(float val) {
  _lcd_vamp_group_data *tmp = (_lcd_vamp_group_data *)__lcd_V.priv;
  _lcd_val_3DP(tmp->digits, tmp->dp, NULL, val);
}
inline void _lcd_V_gfx(Lcd_Gfx seg, Lcd_seg_state st) {
  assert(seg <= LCD_ICON_LV3);
  _lcd_iconSet(&__lcd_vamp_V_privData.unit, st);
}

inline void _lcd_A_disp(Lcd_seg_state state) {
  // TODO: Implement
  // _lcd_CGdisp(&__lcd_A, state);
}
inline void _lcd_A_val(float val) {
  _lcd_vamp_group_data *tmp = (_lcd_vamp_group_data *)__lcd_A.priv;
  _lcd_val_3DP(tmp->digits, tmp->dp, &tmp->opt_mA, val);
}

void _lcd_A_gfx(Lcd_Gfx seg, Lcd_seg_state st) {
  assert(seg <= LCD_ICON_LV3);
  _lcd_iconSet(&__lcd_vamp_A_privData.unit, st);
}

// Routing Function Shims
__LCD_CONTROL_GROUP_ROUTING_SHIMS(cg1)
__LCD_CONTROL_GROUP_ROUTING_SHIMS(cg2)
__LCD_CONTROL_GROUP_ROUTING_SHIMS(cg3)
__LCD_CONTROL_GROUP_ROUTING_SHIMS(cg4)

void _lcd_CGdisp(const lcd_ctrlGrp *grp, Lcd_seg_state st) {
  // TODO: Implement
  return;
}

void _lcd_val_3DP(const _lcd_sev_seg_char *digits, _lcd_icon *dp,
                  const _lcd_icon *mA, float val) {

  _lcd_icon *activeDP;
  int32_t targetTenPow, sigFigs, basisTenPow;

  // Eliminate all DPs to clear the deck
  for (int i = 0; i < LCD_VA_DP_N; i++) {
    _lcd_iconSet(&dp[i], OFF);
  }

  // Calculate the bigness of the number
  int32_t ord = (int32_t)floorf(log10(fabsf(val)));

  // We've got a pointer to a mA, rescale the number and set the icon
  if (mA) {
    if (ord < 0) {
      ord += 3; // Because we are multiplying by 1k.
      val *= 1000.0f;
      _lcd_iconSet(mA, ON);
    } else {
      _lcd_iconSet(mA, OFF);
    }
  }

  if (val >= 0) {
    targetTenPow = LCD_NUM_GROUP_N - 1;
  } else {
    targetTenPow = LCD_NUM_GROUP_N - 2;
  }

  basisTenPow = ((targetTenPow - ord) > targetTenPow) ? targetTenPow
                                                      : (targetTenPow - ord);

  sigFigs = (int32_t)roundf((val * powf(10.0f, (float)(basisTenPow))));

  if (sigFigs < 1000) {
    // If number is sufficiently tiny in magnitude, pad it with zeros by
    // adding a number that will never be rendered.
    sigFigs += (int32_t)powf(10, LCD_DIG_N);
  }

  switch (ord) {
  case 3:
    activeDP = NULL; // Numbers >=1000
    break;
  case 2:
    activeDP = &dp[0]; // 100 -> 1000
    break;
  case 1:
    activeDP = &dp[1]; // 10 -> 100
    break;
  default:
    activeDP = &dp[2]; // 0 -> 10
    break;
  }
  if (sigFigs == 0) {
    activeDP = &dp[0];
  }
  _lcd_display_digits(digits, activeDP, sigFigs);
}

void _lcd_val_1DP(const _lcd_sev_seg_char *digits, _lcd_icon *dp, float val) {
  _lcd_icon *activeDP = NULL;
  int32_t sigFigs = 0;

  _lcd_iconSet(&dp[0], OFF);

  // Constrain the values differently if the number is + or -
  // because we have to make extra room for the "-" in the negative case.
  if (val >= 0.0f) {
    if (val < 1000.0f && (fabsf(val - 1000.0f) > 0.01f)) {
      val *= 10.0f;
      activeDP = &dp[0];
    } else if (val > 9999.9f) {
      val = 9999.0f;
    }
  } else {
    if (val > -100.0f) {
      val *= 10.0f;
      activeDP = &dp[0];
    } else if (val < -999.0f) {
      val = -999.0f;
    }
  }
  // sigFigs = (int32_t)roundf(fabs(val));
  sigFigs = (int32_t)roundf(val);

  if (sigFigs == 0) {
    activeDP = &dp[0];
  }

  _lcd_display_digits(digits, activeDP, sigFigs);
}

void _lcd_display_digits(const _lcd_sev_seg_char *digits, const _lcd_icon *dp,
                         int32_t val) {

  LCD_Char segmentValues[LCD_DIG_N] = {_Off, _Off, _Off, _Off};
  LCDUpdateQueue msg;

  uint8_t segBits = 0;
  int32_t digit;

  if (val == 0) {
    segmentValues[LCD_DIG_N - 2] = _0;
    segmentValues[LCD_DIG_N - 1] = _0;
  }

  if (val < 0) {
    segmentValues[0] = _Dash;
  }

  val = abs(val);

  if (val > 0) {
    for (int i = LCD_DIG_N - 1; i >= 0; i--) {
      digit = val % 10;
      val /= 10;
      switch (digit) {
      case 0:
        if (i == (LCD_DIG_N - 2) || val != 0) {
          segmentValues[i] = _0;
        }
        break;
      case 1:
        segmentValues[i] = _1;
        break;
      case 2:
        segmentValues[i] = _2;
        break;
      case 3:
        segmentValues[i] = _3;
        break;
      case 4:
        segmentValues[i] = _4;
        break;
      case 5:
        segmentValues[i] = _5;
        break;
      case 6:
        segmentValues[i] = _6;
        break;
      case 7:
        segmentValues[i] = _7;
        break;
      case 8:
        segmentValues[i] = _8;
        break;
      case 9:
        segmentValues[i] = _9;
        break;
      }
      // Quit the loop early if we are out of digits to display.
      if (val == 0 && i != LCD_DIG_N - 1) {
        break;
      }
    }
  }

  // Change the LCD's memory bank.
  if (dp) {
    _lcd_iconSet(dp, ON);
  }

  for (int i = 0; i < LCD_DIG_N; i++) {
    segBits = 0;
    for (int j = 0; j < LCD_CHAR_SEG_COUNT; j++) {
      if ((int)segmentValues[i] & 1 << j)
        segBits |= digits[i].char_map_var[j];
    }
    msg.index = digits[i].mem_idx;
    msg.data = segBits;
    msg.mask = 0xFE;
    xQueueSend(xLCDUpdateQueue, &msg, pdMS_TO_TICKS(10));
  }
}

void _lcd_iconSet(const _lcd_icon *ico, Lcd_seg_state st) {
  uint8_t data;
  // LCD_MEMORY is READ ONLY.
  switch (st) {
  case ON:
    data = LCD_MEMORY[ico->idx] | (uint8_t)(1 << ico->bit);
    break;
  case OFF:
    data = LCD_MEMORY[ico->idx] & ~(uint8_t)(1 << ico->bit);
    break;
  case TOG:
    data = LCD_MEMORY[ico->idx] ^ (uint8_t)(1 << ico->bit);
    break;
  }
  LCDUpdateQueue msg = {
      .index = ico->idx, .mask = (uint8_t)(1 << ico->bit), .data = data};
  xQueueSend(xLCDUpdateQueue, &msg, pdMS_TO_TICKS(10));
  return;
}

const lcd_ctrlGrp *const lcd_CtlGrp_init(Lcd_CtlGrp_enum group) {
  const lcd_ctrlGrp *return_val;
  switch (group) {
  case LCD_CG1:
    return_val = &__lcd_cg1;
    break;
  case LCD_CG2:
    return_val = &__lcd_cg2;
    break;
  case LCD_CG3:
    return_val = &__lcd_cg3;
    break;
  case LCD_CG4:
    return_val = &__lcd_cg4;
    break;
  case LCD_CG_BEGIN:
  case LCD_CG_END:
  default:
    assert(0);
    break;
  }
  return return_val;
}

const lcd_VltAmpGrp *lcd_VAMP_init(Lcd_VAGrp group) {
  const lcd_VltAmpGrp *return_val;
  switch (group) {
  case LCD_VAMP_V:
    return_val = &__lcd_V;
    break;
  case LCD_VAMP_A:
    return_val = &__lcd_A;
    break;
  case LCD_VAMP_BEGIN:
  case LCD_VAMP_END:
  default:
    assert(0);
    break;
  }
  return return_val;
}

void lcd_init() {
  xLCDUpdateQueue = xQueueCreate(LCD_UPD_QUEUE_N, sizeof(xLCDUpdateQueue));
  configASSERT(xLCDUpdateQueue);
#ifndef NDEBUG
  vQueueAddToRegistry(xLCDUpdateQueue, "LCD Updates");
#endif // !NDEBUG

  ht1623_init();
}

void lcdTask(void *params) {
  LCDUpdateQueue message;
  const uint32_t arrSz = sizeof(LCD_MEMORY) / sizeof(uint8_t);
  for (;;) {
    while (xQueueReceive(xLCDUpdateQueue, &message, pdMS_TO_TICKS(100))) {
      // Try to merge the message in to memory.
      LCD_MEMORY[message.index] &= ~(message.mask);
      LCD_MEMORY[message.index] |= message.data;
      memoryState |= (uint64_t)1 << message.index; // Always sets bit.
    }

    for (uint64_t i = 0; i < LCD_SEGMENTS; i++) {
      if (memoryState & ((uint64_t)1 << i)) {
        ht1623_senddata(i * 2, LCD_MEMORY[i]);
        memoryState ^= (uint64_t)1 << i; // So XOR always clears.
      }
    }
  }
}
