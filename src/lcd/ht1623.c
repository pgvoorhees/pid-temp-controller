// This is an independent project of an individual developer. Dear PVS-Studio,
// please check it. PVS-Studio Static Code Analyzer for C, C++ and C#:
// http://www.viva64.com
/***********************************************************************
 * HT1623_LedMatrix.cpp - Arduino library for Holtek HT1632 LED driver chip,
 *     As implemented on the Sure Electronics DE-DP10X display board
 *       (8 x 32 dot matrix LED module.)
 *
 * Original code by:
 * Nov, 2008 by Bill Westfield ("WestfW")
 *   Copyrighted and distributed under the terms of the Berkely license
 *   (copy freely, but include this notice of original author.)
 *
 * Adapted for 8x32 display by FlorinC.
 *
 * Library Created and updated by Andrew Lindsay October/November 2009
 ***********************************************************************/

#include "ht1623.h"

#include "stm32f4xx.h"
#include <inttypes.h>

#define _BV(bit) (1 << (bit))
typedef enum {
  MSB,
  LSB,
} bit_order;

// class HT1623_LedMatrix
void chipselect(void);
void chipfree(void);
void writebits(uint8_t, uint8_t, bit_order);
void sendcmd(uint8_t);

/*
 * commands written to the chip consist of a 3 bit "ID", followed by
 * either 9 bits of "Command code" or 7 bits of address + 4 bits of data.
 */
#define HT1623_ID_CMD 4 /* ID = 100 - Commands */
#define HT1623_ID_RD 6  /* ID = 110 - Read RAM */
#define HT1623_ID_WR 5  /* ID = 101 - Write RAM */

#define HT1623_CMD_SYSDIS 0x00 /* CMD= 0000-0000-x Turn off oscil */
#define HT1623_CMD_SYSON 0x01  /* CMD= 0000-0001-x Enable system oscil */
#define HT1623_CMD_LCDOFF 0x02 /* CMD= 0000-0010-x LED duty cycle gen off */
#define HT1623_CMD_LCDON 0x03  /* CMD= 0000-0011-x LEDs ON */
#define HT1623_CMD_TIMOFF 0x04 /* CMD= 0000-1000-x Blink ON */
#define HT1623_CMD_WDTOFF 0x05
#define HT1623_CMD_TIMON 0x06
#define HT1623_CMD_WDTON 0x07
#define HT1623_CMD_TONOFF 0x08
#define HT1623_CMD_CLRTIM 0x0D
#define HT1623_CMD_CLRWDT 0x0F
#define HT1623_CMD_INT32K 0x18
#define HT1623_CMD_EXT32K 0x1C
#define HT1623_CMD_TON4K 0x40
#define HT1623_CMD_TON2K 0x60
#define HT1623_CMD_IRQOFF 0x80
#define HT1623_CMD_IRQON 0x88

// helper macros
#define output_low(port, pin) ((port)->BSRRH |= (1 << (pin)))
#define output_high(port, pin) ((port)->BSRRL |= (1 << (pin)))

void ht1623_init() {

  RCC->AHB1ENR = RCC_AHB1ENR_GPIOAEN;

  HT1623_WRCLK_PORT->ODR |= 1 << HT1623_WRCLK;
  chipfree();

  GPIOA->MODER |=
      GPIO_MODER_MODER4_0 | GPIO_MODER_MODER5_0 | GPIO_MODER_MODER7_0;

  sendcmd(HT1623_CMD_SYSON);
  sendcmd(HT1623_CMD_TIMON);
  sendcmd(HT1623_CMD_LCDON);

  ht1623_clearMemory();
}

inline void delay_ticks(int32_t ticks) {
  while (ticks--) {
    __ASM("nop;");
  }
}

void ht1623_clearMemory(void) {
  for (int i = 0; i < 96; i++) {
    ht1623_senddata(i, 0x00);
    delay_ticks(DLYCT);
  }
}

/***********************************************************************
 * chipselect / chipfree
 * Select or de-select a particular ht1632 chip.
 * De-selecting a chip ends the commands being sent to a chip.
 * CD pins are active-low; writing 0 to the pin selects the chip.
 ***********************************************************************/
void chipselect() {
  output_low(HT1623_CS_PORT, HT1623_CS);
  delay_ticks(DLYCT);
}

void chipfree() {
  output_high(HT1623_DATA_PORT, HT1623_DATA);
  delay_ticks(DLYCT);
  output_high(HT1623_WRCLK_PORT, HT1623_WRCLK);
  delay_ticks(DLYCT);
  output_high(HT1623_CS_PORT, HT1623_CS);
  delay_ticks(DLYCT);
}

/*
 * writebits
 * Write bits to h1632 on pins HT1623_DATA, HT1623_WRCLK
 * Chip is assumed to already be chip-selected
 * Bits are shifted out from MSB to LSB, with the first bit sent
 * being (bits & firstbit), shifted till firsbit is zero.
 */
void writebits(uint8_t bits, uint8_t firstbit, bit_order order) {
  int32_t bit;
  int32_t iters = __builtin_ctz(firstbit);
  switch (order) {
  case MSB:
    bit = firstbit;
    break;
  case LSB:
    bit = 1;
    break;
  }
  for (int32_t i = 0; i <= iters; i++) {
    output_low(HT1623_WRCLK_PORT, HT1623_WRCLK);
    delay_ticks(DLYCT);
    if (bits & bit) {
      output_high(HT1623_DATA_PORT, HT1623_DATA);
    } else {
      output_low(HT1623_DATA_PORT, HT1623_DATA);
    }
    delay_ticks(DLYCT);
    output_high(HT1623_WRCLK_PORT, HT1623_WRCLK);
    delay_ticks(DLYCT + DLYCT);
    switch (order) {
    case MSB:
      bit >>= 1;
      break;
    case LSB:
      bit <<= 1;
      break;
    }
  }
}

/*
 * sendcmd
 * Send a command to the ht1632 chip.
 * A command consists of a 3-bit "CMD" ID, an 8bit command, and
 * one "don't care bit".
 *   Select 1 0 0 c7 c6 c5 c4 c3 c2 c1 c0 xx Free
 */
void sendcmd(uint8_t command) {
  chipselect();                        // Select chip
  writebits(HT1623_ID_CMD, 0x04, MSB); // 1<<2);  // send 3 bits of id: COMMMAND
  writebits(command, 0x80, MSB);       // 1<<7);  // send the actual command
  writebits(0, 0x1, MSB); /* one extra dont-care bit in commands. */
  chipfree();             // done
}

/*
 * senddata
 * send a nibble (4 bits) of data to a particular memory location of the
 * ht1632.  The command has 3 bit ID, 7 bits of address, and 4 bits of data.
 *    Select 1 0 1 A6 A5 A4 A3 A2 A1 A0 D0 D1 D2 D3 Free
 * Note that the address is sent MSB first, while the data is sent LSB first!
 * This means that somewhere a bit reversal will have to be done to get
 * zero-based addressing of words and dots within words.
 */
void ht1623_senddata(uint8_t address, uint8_t data) {
  chipselect();                       // Select chip
  writebits(HT1623_ID_WR, 0x04, MSB); // 1<<2);  // send ID: WRITE to RAM
  writebits(address, 0x40, MSB);      // 1<<6); // Send address
  writebits(data, 0x80, LSB);         // 1<<3); // send 4 bits of data
  chipfree();                         // done
}
