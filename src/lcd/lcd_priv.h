#include "assert.h"
#include "inc/lcd.h"
#include "math.h"
#include "stdint.h"
#include "stdlib.h"
#include "string.h"

#include "FreeRTOS.h"
#include "event_groups.h"
#include "ht1623.h"
#include "queue.h"
#include "semphr.h"
#include "timers.h"

#define LCD_SEGMENTS 48
#define LCD_DEAD (LCD_SEGMENTS)
#define LCD_COMMONS 8
#define LCD_CHAR_SEG_COUNT 7

#define LCD_NUM_GROUP_N 4
#define LCD_CTRL_GRP_DP_N 1
#define LCD_CTRL_GRP_GFX_MX 3
#define LCD_VA_DP_N 3
#define LCD_DIG_N 4 // Count of digits making up a number.

// Reserve a byte that will not be transmitted.
// This is for slop, it allows us to write segments
// which might be provisioned but not actually exist.
// __attribute__ ((section (".memory_quarantine")))
static volatile uint8_t LCD_MEMORY[LCD_SEGMENTS + 1] = {
    0xF0, 0XEE, 0xF0, 0XEE, 0xC0, 0x00, 0x00, 0XEF, 0XEF, 0xF1,
    0XEE, 0XEE, 0XEE, 0XEF, 0XEF, 0XEE, 0XEE, 0XEF, 0XEE, 0XEE,
    0x00, 0x00, 0x00, 0x01, 0XEE, 0XEF, 0x01, 0x00, 0x00, 0x00,
    0XEF, 0XEE, 0XEE, 0x00, 0x01, 0x01, 0x00, 0XEF, 0XEE, 0XEE,
    0x00, 0x01, 0x00, 0x00, 0x00, 0XEF, 0XEE, 0x01, 0x00};
uint64_t memoryState = 0xFFFFFFFFFFFF;

#define LCD_UPD_QUEUE_N 20
typedef struct {
  uint8_t index;
  uint8_t mask;
  uint8_t data;
  uint32_t parentStruct;
} LCDUpdateQueue;
// uint8_t __lcdupdqueubuffer[sizeof(LCDUpdateQueue) * LCD_UPD_QUEUE_N] = {0};
QueueHandle_t xLCDUpdateQueue;
TimerHandle_t xLCDUpdateTimer;
// static StaticQueue_t xLCDStaticQueue;

// Hardware mapped type definitions.
static const uint8_t LCD_CHAR_SEG_MAP_A[LCD_CHAR_SEG_COUNT] = {
    0b10000000, // A - COM8
    0b01000000, // B - COM7
    0b00001000, // C - COM4
    0b00000010, // D - COM2
    0b00000100, // E - COM3
    0b00100000, // F - COM6
    0b00010000, // G - COM5
};

static const uint8_t LCD_CHAR_SEG_MAP_B[LCD_CHAR_SEG_COUNT] = {
    0b00000010, // A - COM2
    0b00000100, // B - COM3
    0b00100000, // C - COM6
    0b10000000, // D - COM8
    0b01000000, // E - COM7
    0b00001000, // F - COM4
    0b00010000, // G - COM5
};

/* clang-format off */
typedef enum {
    // 0-9 A-F
    _0 = 0x3F, _1 = 0x06, _2 = 0x5B, _3 = 0x4F,
    _4 = 0x66, _5 = 0x6D, _6 = 0x7D, _7 = 0x07,
    _8 = 0x7F, _9 = 0x6F, _A = 0x77, _B = 0x7C,
    _C = 0x39, _D = 0x5E, _E = 0x79, _F = 0x71,
    // Other Alphas
    _G = 0x3D, _H = 0x74, _L = 0x38, _N = 0x54,
    _O = 0x5C, _P = 0x73, _R = 0x50, _U = 0x1C,
    _Y = 0x6E,
    // Catch all
    _Dash = 0x40,
    // Off
    _Off = 0x0,
} LCD_Char;
/* clang-format on */

typedef struct {
  uint8_t mem_idx;
  const uint8_t(*const char_map_var);
} _lcd_sev_seg_char;

typedef struct LCD_Icon_t {
  uint8_t idx;
  uint8_t bit;
} _lcd_icon;

typedef struct {
  _lcd_sev_seg_char digits[LCD_NUM_GROUP_N];
  _lcd_icon dp[LCD_VA_DP_N];
  _lcd_icon unit;
  _lcd_icon opt_mA;
} _lcd_vamp_group_data;

typedef struct {
  _lcd_icon unit;

  _lcd_sev_seg_char pv_digits[LCD_NUM_GROUP_N];
  _lcd_icon pv_dp;
  _lcd_icon pv_ico;

  _lcd_sev_seg_char sv_digits[LCD_NUM_GROUP_N];
  _lcd_icon sv_dp;
  _lcd_icon sv_ico;

  _lcd_icon heat;
  _lcd_icon gfx[LCD_CTRL_GRP_GFX_MX];
} _lcd_control_group_data;

void _lcd_iconSet(const _lcd_icon(*), Lcd_seg_state);
void _lcd_val_1DP(const _lcd_sev_seg_char(*), _lcd_icon(*), float);
void _lcd_val_3DP(const _lcd_sev_seg_char(*), _lcd_icon(*), const _lcd_icon(*),
                  float);
void _lcd_CGdisp(const lcd_ctrlGrp(*), Lcd_seg_state);

#define __LCD_VAMP_PUB_STRUCT(name)                                            \
  lcd_VltAmpGrp __lcd_##name = {                                               \
      .display = _lcd_##name##_disp,                                           \
      .val = _lcd_##name##_val,                                                \
      .ico = _lcd_##name##_ico,                                                \
      .priv = &__lcd_vamp_##name##_privData,                                   \
      .chars = _lcd_##name##_chars,                                            \
  };

#define __LCD_VAMP_SHIM_PROTOTYPE(name)                                        \
  void _lcd_##name##_disp(Lcd_seg_state);                                      \
  void _lcd_##name##_val(float);                                               \
  void _lcd_##name##_ico(Lcd_Ico_Cmd_VA gfx,                                   \
                         Lcd_seg_state st){/*TODO: Implement*/};               \
  void _lcd_##name##_chars(char *chars){/*TODO: Implement*/};

__LCD_VAMP_SHIM_PROTOTYPE(A)
__LCD_VAMP_SHIM_PROTOTYPE(V)

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wmissing-field-initializers"
const _lcd_vamp_group_data __lcd_vamp_A_privData = {
    .digits[0].mem_idx = 17,
    .digits[0].char_map_var = LCD_CHAR_SEG_MAP_A,
    .digits[1].mem_idx = 16,
    .digits[1].char_map_var = LCD_CHAR_SEG_MAP_A,
    .digits[2].mem_idx = 15,
    .digits[2].char_map_var = LCD_CHAR_SEG_MAP_A,
    .digits[3].mem_idx = 14,
    .digits[3].char_map_var = LCD_CHAR_SEG_MAP_A,
    .dp[0].idx = 15,
    .dp[0].bit = 0,
    .dp[1].idx = 16,
    .dp[1].bit = 0,
    .dp[2].idx = 17,
    .dp[2].bit = 0,
    .unit.idx = 13,
    .unit.bit = 0,
    .opt_mA.idx = 14,
    .opt_mA.bit = 0,
};
__LCD_VAMP_PUB_STRUCT(A)
#pragma GCC diagnostic pop

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wmissing-field-initializers"
const _lcd_vamp_group_data __lcd_vamp_V_privData = {
    .digits[0].mem_idx = 13,
    .digits[0].char_map_var = LCD_CHAR_SEG_MAP_A,
    .digits[1].mem_idx = 12,
    .digits[1].char_map_var = LCD_CHAR_SEG_MAP_A,
    .digits[2].mem_idx = 11,
    .digits[2].char_map_var = LCD_CHAR_SEG_MAP_A,
    .digits[3].mem_idx = 10,
    .digits[3].char_map_var = LCD_CHAR_SEG_MAP_A,
    .dp[0].idx = 10,
    .dp[0].bit = 0,
    .dp[1].idx = 11,
    .dp[1].bit = 0,
    .dp[2].idx = 12,
    .dp[2].bit = 0,
    .unit.idx = 9,
    .unit.bit = 4,
    .opt_mA.idx = LCD_DEAD,
    .opt_mA.bit = 0,
};
__LCD_VAMP_PUB_STRUCT(V)
#pragma GCC diagnostic pop

#define __LCD_CG_PUB_STRUCT(name)                                              \
  lcd_ctrlGrp __lcd_##name = {                                                 \
      .display = _lcd_##name##_disp,                                           \
      .pVal = _lcd_##name##_pval,                                              \
      .sVal = _lcd_##name##_sval,                                              \
      .heat = _lcd_##name##_heat,                                              \
      .gfx = _lcd_##name##_gfx,                                                \
      .unit = _lcd_##name##_indicator,                                         \
      .priv = &__lcd_##name##_privData,                                        \
  };

#define __LCD_CG_SHIM_PROTOTYPE(name)                                          \
  void _lcd_##name##_disp(Lcd_seg_state);                                      \
  void _lcd_##name##_pval(float);                                              \
  void _lcd_##name##_sval(float);                                              \
  void _lcd_##name##_heat(Lcd_seg_state);                                      \
  void _lcd_##name##_gfx(Lcd_Gfx, Lcd_seg_state);                              \
  void _lcd_##name##_indicator(Lcd_Ico_Cmd_CtlGrp, Lcd_seg_state);

__LCD_CG_SHIM_PROTOTYPE(cg1)
__LCD_CG_SHIM_PROTOTYPE(cg2)
__LCD_CG_SHIM_PROTOTYPE(cg3)
__LCD_CG_SHIM_PROTOTYPE(cg4)

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wmissing-field-initializers"
const const _lcd_control_group_data __lcd_cg1_privData = {
    .pv_digits[0].mem_idx = 21,
    .pv_digits[0].char_map_var = LCD_CHAR_SEG_MAP_B,
    .pv_digits[1].mem_idx = 20,
    .pv_digits[1].char_map_var = LCD_CHAR_SEG_MAP_B,
    .pv_digits[2].mem_idx = 19,
    .pv_digits[2].char_map_var = LCD_CHAR_SEG_MAP_B,
    .pv_digits[3].mem_idx = 18,
    .pv_digits[3].char_map_var = LCD_CHAR_SEG_MAP_B,
    .pv_dp.idx = 9,
    .pv_dp.bit = 7,
    .pv_ico.idx = 9,
    .pv_ico.bit = 6,

    .sv_digits[0].mem_idx = 5,
    .sv_digits[0].char_map_var = LCD_CHAR_SEG_MAP_A,
    .sv_digits[1].mem_idx = 6,
    .sv_digits[1].char_map_var = LCD_CHAR_SEG_MAP_A,
    .sv_digits[2].mem_idx = 7,
    .sv_digits[2].char_map_var = LCD_CHAR_SEG_MAP_A,
    .sv_digits[3].mem_idx = 8,
    .sv_digits[3].char_map_var = LCD_CHAR_SEG_MAP_A,
    .sv_dp.idx = 7,
    .sv_dp.bit = 0,
    .sv_ico.idx = 8,
    .sv_ico.bit = 0,

    .unit.idx = 9,
    .unit.bit = 5,

    .heat.idx = 22,
    .heat.bit = 0,

    .gfx[0].idx = 23,
    .gfx[0].bit = 0,
    .gfx[1].idx = 21,
    .gfx[1].bit = 0,
    .gfx[2].idx = LCD_DEAD,
    .gfx[2].bit = 0,
};
__LCD_CG_PUB_STRUCT(cg1)
#pragma GCC diagnostic pop

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wmissing-field-initializers"
const _lcd_control_group_data __lcd_cg2_privData = {
    .pv_digits[0].mem_idx = 27,
    .pv_digits[0].char_map_var = LCD_CHAR_SEG_MAP_B,
    .pv_digits[1].mem_idx = 26,
    .pv_digits[1].char_map_var = LCD_CHAR_SEG_MAP_B,
    .pv_digits[2].mem_idx = 25,
    .pv_digits[2].char_map_var = LCD_CHAR_SEG_MAP_B,
    .pv_digits[3].mem_idx = 24,
    .pv_digits[3].char_map_var = LCD_CHAR_SEG_MAP_B,
    .pv_dp.idx = 4,
    .pv_dp.bit = 7,
    .pv_ico.idx = 4,
    .pv_ico.bit = 6,

    .sv_digits[0].mem_idx = 28,
    .sv_digits[0].char_map_var = LCD_CHAR_SEG_MAP_A,
    .sv_digits[1].mem_idx = 29,
    .sv_digits[1].char_map_var = LCD_CHAR_SEG_MAP_A,
    .sv_digits[2].mem_idx = 30,
    .sv_digits[2].char_map_var = LCD_CHAR_SEG_MAP_A,
    .sv_digits[3].mem_idx = 3,
    .sv_digits[3].char_map_var = LCD_CHAR_SEG_MAP_A,
    .sv_dp.idx = 30,
    .sv_dp.bit = 0,
    .sv_ico.idx = 2,
    .sv_ico.bit = 4,

    .unit.idx = 4,
    .unit.bit = 6,

    .heat.idx = 27,
    .heat.bit = 0,

    .gfx[0].idx = 25,
    .gfx[0].bit = 0,
    .gfx[1].idx = 26,
    .gfx[1].bit = 0,
    .gfx[2].idx = LCD_DEAD,
    .gfx[2].bit = 0,
};
__LCD_CG_PUB_STRUCT(cg2)
#pragma GCC diagnostic pop

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wmissing-field-initializers"
const _lcd_control_group_data __lcd_cg3_privData = {
    .pv_digits[0].mem_idx = 34,
    .pv_digits[0].char_map_var = LCD_CHAR_SEG_MAP_B,
    .pv_digits[1].mem_idx = 33,
    .pv_digits[1].char_map_var = LCD_CHAR_SEG_MAP_B,
    .pv_digits[2].mem_idx = 32,
    .pv_digits[2].char_map_var = LCD_CHAR_SEG_MAP_B,
    .pv_digits[3].mem_idx = 31,
    .pv_digits[3].char_map_var = LCD_CHAR_SEG_MAP_B,
    .pv_dp.idx = 2,
    .pv_dp.bit = 7,
    .pv_ico.idx = 2,
    .pv_ico.bit = 6,

    .sv_digits[0].mem_idx = 35,
    .sv_digits[0].char_map_var = LCD_CHAR_SEG_MAP_A,
    .sv_digits[1].mem_idx = 36,
    .sv_digits[1].char_map_var = LCD_CHAR_SEG_MAP_A,
    .sv_digits[2].mem_idx = 37,
    .sv_digits[2].char_map_var = LCD_CHAR_SEG_MAP_A,
    .sv_digits[3].mem_idx = 1,
    .sv_digits[3].char_map_var = LCD_CHAR_SEG_MAP_A,
    .sv_dp.idx = 37,
    .sv_dp.bit = 0,
    .sv_ico.idx = 0,
    .sv_ico.bit = 4,

    .unit.idx = 2,
    .unit.bit = 5,

    .heat.idx = 36,
    .heat.bit = 0,

    .gfx[0].idx = 33,
    .gfx[0].bit = 0,
    .gfx[1].idx = 34,
    .gfx[1].bit = 0,
    .gfx[2].idx = 35,
    .gfx[2].bit = 0,
};
__LCD_CG_PUB_STRUCT(cg3)
#pragma GCC diagnostic pop

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wmissing-field-initializers"
const _lcd_control_group_data __lcd_cg4_privData = {
    .pv_digits[0].mem_idx = 42,
    .pv_digits[0].char_map_var = LCD_CHAR_SEG_MAP_B,
    .pv_digits[1].mem_idx = 41,
    .pv_digits[1].char_map_var = LCD_CHAR_SEG_MAP_B,
    .pv_digits[2].mem_idx = 39,
    .pv_digits[2].char_map_var = LCD_CHAR_SEG_MAP_B,
    .pv_digits[3].mem_idx = 38,
    .pv_digits[3].char_map_var = LCD_CHAR_SEG_MAP_B,
    .pv_dp.idx = 0,
    .pv_dp.bit = 7,
    .pv_ico.idx = 0,
    .pv_ico.bit = 6,

    .sv_digits[0].mem_idx = 43,
    .sv_digits[0].char_map_var = LCD_CHAR_SEG_MAP_A,
    .sv_digits[1].mem_idx = 44,
    .sv_digits[1].char_map_var = LCD_CHAR_SEG_MAP_A,
    .sv_digits[2].mem_idx = 45,
    .sv_digits[2].char_map_var = LCD_CHAR_SEG_MAP_A,
    .sv_digits[3].mem_idx = 46,
    .sv_digits[3].char_map_var = LCD_CHAR_SEG_MAP_A,
    .sv_dp.idx = 45,
    .sv_dp.bit = 0,
    .sv_ico.idx = 47,
    .sv_ico.bit = 0,

    .unit.idx = 0,
    .unit.bit = 5,

    .heat.idx = 42,
    .heat.bit = 0,

    .gfx[0].idx = 40,
    .gfx[0].bit = 0,
    .gfx[1].idx = 41,
    .gfx[1].bit = 0,
    .gfx[2].idx = LCD_DEAD,
    .gfx[2].bit = 0,
};
__LCD_CG_PUB_STRUCT(cg4)
#pragma GCC diagnostic pop

void _lcd_display_digits(const _lcd_sev_seg_char *digits, const _lcd_icon *dp,
                         int32_t val);
void _lcd_display_refresh_callback(TimerHandle_t xTimer);