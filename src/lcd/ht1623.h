/*
 * ht1623.h
 *
 *  Created on: May 2, 2019
 *      Author: Peter
 */

#ifndef HT1623_H_
#define HT1623_H_

#include <inttypes.h>

/*
 * Set these constants to the values of the pins connected to the
 * SureElectronics Module NOTE - these are different from the original demo code
 * by westfw
 *
 * Use Pin Mappings 8-11 for CS1 to 4, 12 for Data and 13 for Clock
 * Use mixture of #define and variables.
 * Pin numbers are for setup and port identifiers are for direct port writes.
 */

#define HT1623_DATA 7           // Data pin
#define HT1623_DATA_PORT GPIOA  // Data port
#define HT1623_WRCLK 5          // Write clock pin
#define HT1623_WRCLK_PORT GPIOA // Write clock port
#define HT1623_CS 4             // Write clock pin
#define HT1623_CS_PORT GPIOA    // Write clock port
#define DLYCT 1

// Init/Clear/position functions
void ht1623_init();
void delay_ticks(int32_t ticks);
void ht1623_senddata(uint8_t, uint8_t);
void ht1623_clearMemory(void);

#endif /* HT1623_H_ */
