#ifndef __UART
#define __UART

void uart_Initialize(
    UBaseType_t prio, TaskHandle_t* hnd_uartTx, TaskHandle_t* hnd_uartRx);

#endif // UART
