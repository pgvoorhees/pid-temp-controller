#ifndef __SYSTEM
#define __SYSTEM

#include "stm32f4xx.h"
#include "stm32f4xx_gpio.h"

#define SYS_WDT_WAIT_TICKS  10

typedef struct {
  GPIO_TypeDef *port;
  uint16_t pin;
} Pin;

void SWO_Init(uint32_t portBits, uint32_t cpuCoreFreqHz);
void SWO_PrintString(const char *s, uint8_t portNumber);

#endif // SYSTEM
