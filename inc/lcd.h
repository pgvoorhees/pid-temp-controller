typedef enum {
  OFF,
  ON,
  TOG,
} Lcd_seg_state;

typedef enum {
  VOLT,
  AMP,
  mAMP,
} Lcd_Ico_Cmd_VA;

typedef enum {
  UNITS,
  PVAL,
  SVAL,
} Lcd_Ico_Cmd_CtlGrp;

typedef enum {
  LOAD,
  CELL,
  FUEL,
} Lcd_Ico_Cmd_Status;

typedef enum {
  LCD_ICON_LV1 = 0, // Most enclosed
  LCD_ICON_LV2,
  LCD_ICON_LV3, // Least enclosed -- Might not exist
} Lcd_Gfx;

typedef enum {
  INVALID_ICON = -1,
  ERR = 0,
  OK,
} Lcd_ret;

typedef enum {
  LCD_CG_BEGIN = -1,
  LCD_CG1,
  LCD_CG2,
  LCD_CG3,
  LCD_CG4,
  LCD_CG_END,
} Lcd_CtlGrp_enum;

typedef enum {
  LCD_VAMP_BEGIN = -1,
  LCD_VAMP_V,
  LCD_VAMP_A,
  LCD_VAMP_END,
} Lcd_VAGrp;

Lcd_ret Lcd_init();

typedef struct lcd_VAgp_t {
  void (*display)(Lcd_seg_state state);
  void (*val)(float val);
  void (*chars)(char *chars);
  void (*ico)(Lcd_Ico_Cmd_VA icon, Lcd_seg_state state);
  const void *priv;
} lcd_VltAmpGrp;
const lcd_VltAmpGrp *lcd_VAMP_init(Lcd_VAGrp);

typedef struct lcd_controlgroup_t {
  void (*display)(Lcd_seg_state state);
  void (*pVal)(float val);
  void (*sVal)(float val);
  void (*heat)(Lcd_seg_state state);
  void (*gfx)(Lcd_Gfx icon, Lcd_seg_state state);
  void (*unit)(Lcd_Ico_Cmd_CtlGrp unit, Lcd_seg_state state);
  const void *const priv;
} lcd_ctrlGrp;
const lcd_ctrlGrp *const lcd_CtlGrp_init(Lcd_CtlGrp_enum);

typedef struct lcd_stat_t {
  void (*gfx)(Lcd_Ico_Cmd_Status, Lcd_seg_state);
  void *priv;
} lcd_stat;
const lcd_stat *const lcd_stat_init(void);

void lcdTask(void *params);
void lcd_init(void);