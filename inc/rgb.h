#ifndef __RGB
#define __RGB

#include "stm32f4xx.h"
#include "stm32f4xx_gpio.h"

typedef enum { RED_PIN, GRN_PIN, BLU_PIN, WHT_PIN } RGBWPins;

#endif // RGB
