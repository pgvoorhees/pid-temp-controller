target_sources("${PROJECT_NAME}"
    PUBLIC "croutine.c"
    PUBLIC "event_groups.c"
    PUBLIC "list.c"
    PUBLIC "queue.c"
    PUBLIC "stream_buffer.c"
    PUBLIC "tasks.c"
    PUBLIC "timers.c"
)

add_subdirectory("portable")